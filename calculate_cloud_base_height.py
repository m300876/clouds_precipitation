#!/usr/bin/env python


####importing packages
import sys
sys.path.append('/work/mh0731/m300876/package')
import icons
from pathlib import Path
import importlib
import numpy as np
import xarray as xr
import intake
import dask
from distributed import Client, progress, wait



icons.prepare_cpu(nworker=1,memory='256GB')


def parse_icon_time(t):
    fraction_of_day = t % 1
    seconds_of_day = int(np.round(24*60*60 * fraction_of_day))
    daystr = str(int(t))
    return np.datetime64(daystr[0:4] + "-" + daystr[4:6] + "-" + daystr[6:8]) \
            + np.timedelta64(seconds_of_day, "s")

def fix_icon_time(ds):
    return ds.assign_coords(time=[parse_icon_time(t) for t in ds.time.values])

def fix_icon_netcdf(ds):
    ds = ds.pipe(fix_icon_time)
    ds = ds.rename({"ncells": "cell"}).squeeze().drop(["height", "height_2"])
    return ds

def fix_vgrid(ds):
    return ds.rename({"height": "halflevel",
        "height_2": "level",
        "ncells": "cell",
        "vertices": "vertex"})

def calcu_min_height(dvar,ddim,threshold):
    cloud_base = ddim.where(dvar >= threshold,99999).min(dim='level',skipna=True)
    return cloud_base.compute()

def open_data(filecatalog,times,levels,time_step):
    cat = intake.open_catalog(filecatalog)
    dset = cat.nextGEMS.dpp0066.atm["3d_ml"].to_dask().pipe(fix_icon_time).sel(time=slice(
        times[0],times[1]),level=slice(levels[0],levels[1])).unify_chunks().resample(time=time_step).mean(
                'time',keep_attrs=True)
    return dset,cat

def open_grid(dataset,catalog,levels):
    grid = catalog.grids[dataset.uuidOfVGrid].to_dask().pipe(fix_vgrid).sel(level=slice(levels[0],levels[1]))
    return grid

def calculate_cloud_base(filecatalog,var,times,outfile,levels=[40,90],time_step='1D',threshold=0.00001):
    dataset,catalog = open_data(filecatalog,times,levels,time_step)
    grid = open_grid(dataset,catalog,levels)
    dvar = dataset[var].chunk({'time':1,'level':-1,'cell':6000000})
    ddim = grid['zg'].chunk({'level':-1,'cell':6000000})
    cloud_base= calcu_min_height(dvar,ddim,threshold)
    return cloud_base.to_netcdf(
            '/scratch/m/m300876/data/dpp0066/clouds/cloud_top_base_dpp0066_'+outfile+'.nc',
            format = 'NETCDF4', mode ='w', group=None)


def calculate_cloud_base_loop(filecatalog,var,times,levels=[40,90],time_step='1D',threshold=0.00001):
    dataset,catalog = open_data(filecatalog,times,levels,time_step)
    grid = open_grid(dataset,catalog,levels)
    dvar = dataset[var].chunk({'time':1,'level':-1,'cell':6000000})
    ddim = grid['zg'].chunk({'level':-1,'cell':6000000})
    for t in range(dvar.time.shape[0]):
        cloud_base= calcu_min_height(dvar.isel(time=t),ddim,threshold)
        years = str(cloud_base['time.year'].values)
        months = str(cloud_base['time.month'].values).zfill(2)
        days = str(cloud_base['time.day'].values).zfill(2)
        cloud_base.to_netcdf(
                '/scratch/m/m300876/data/dpp0066/clouds/cloud_base_height_dpp0066_'+years+months+days+'.nc',
                format = 'NETCDF4', mode ='w', group=None)
    return print('job done')

filecata = '/home/m/m300827/catalogs/catalog.yaml'
var = 'clw'
times = ['2020-02-01','2020-02-04']

save_var = calculate_cloud_base_loop(filecata,var,times,levels=[40,90],time_step='1D',threshold=0.00001)
